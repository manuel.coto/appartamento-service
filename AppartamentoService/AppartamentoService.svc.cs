﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AppartamentoService.Models;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;

namespace AppartamentoService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.

    public class AppartamentoService : IAppartamentoServices
    {
        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret= "43MbXgHOUauz5IBEVQeUZQeiv4bCMibxlP3c5DRI",
            BasePath= "https://appartamento-2bbcf.firebaseio.com/"
        };

        IFirebaseClient client;
        public async Task<string> GetValueAsync(string word)
        {
            return await Task.Factory.StartNew(() => { return "HI" + word; });

        }
        public async Task<string> GetUsersOfFamilyAsync(string code)
        {

            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            List<string> users = new List<string>();
            try
            {
                List<Usuario> usuarios = new List<Usuario>();
                client = new FireSharp.FirebaseClient(config);
                var response = await client.GetTaskAsync("usuario");
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var records = ser.Deserialize<List<Usuario>>(response.Body);

                Parallel.ForEach(records, item =>
                 {
                     if (item != null)
                     {
                         for (int z = 0; z < records.Count; z++)
                         {
                             if (records[z] != null)
                                 if ((item.family.Equals(records[z].family)) && item.family.Equals(code))
                                 {
                                     if (!users.Contains(z.ToString()))
                                     {
                                         users.Add(z.ToString());
                                     }
                                 }
                         }
                     }
                 });


            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            
            return ConvertToString(users);
        }
        public async Task<string> GetUserByMailAsync(string mail)
        {

            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            List<string> users = new List<string>();
            try
            {
                List<Usuario> usuarios = new List<Usuario>();
                client = new FireSharp.FirebaseClient(config);
                var response = await client.GetTaskAsync("usuario");
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var records = ser.Deserialize<List<Usuario>>(response.Body);

                Parallel.ForEach(records, item =>
                 {

                     if (item!=null)
                         {
                             for(int z = 0; z < records.Count; z++)
                             {
                             if (records[z] != null)
                             {
                                 if ((item.correo.Equals(records[z].correo)) && item.correo.Equals(mail))
                                 {
                                         if (!users.Contains(z.ToString()))
                                         {
                                         users.Add(z.ToString());
                                         }
                                 }
                             }

                         }
                         }
                 });


            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            
            return ConvertToString(users);
        }
        private string ConvertToString(List<string> text)
        {
            string returned = "";
            text.ForEach(item =>
            {
                if (returned == "")
                    returned += item;
                else
                    returned += "," + item;
            });
            return returned;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
